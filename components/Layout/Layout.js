import React, { Component } from 'react';
import Head from 'next/head';
import { Col, Container, Row } from 'react-bootstrap';

class Layout extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  componentDidMount() {
  }
  render() {
    const { } = this.state;
    return (
      <React.Fragment>
        <Head>
          <title>Home Page</title>
          <meta name="description" content="" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
        {this.props.children}

        {/* Navbar */}
        <Container>
          <Row>
            <Col xs="12">
              <div className="main_navbar">
                <ul>
                  <li>
                    <a href="javascript:void(0)"><img src="/images/home.png" alt="Home" /></a>
                  </li>
                  <li>
                    <a href="javascript:void(0)"><img src="/images/search.png" alt="Home" /></a>
                  </li>
                  <li>
                    <a href="javascript:void(0)"><img src="/images/add-post.png" alt="Home" /></a>
                  </li>
                  <li>
                    <a href="javascript:void(0)"><img src="/images/notifications.png" alt="Home" /></a>
                  </li>
                  <li>
                    <a href="javascript:void(0)"><img src="/images/charts.png" alt="Home" /></a>
                  </li>
                </ul>
              </div>
            </Col>
          </Row>
        </Container>
      </React.Fragment >
    );
  }
}

export default Layout;



