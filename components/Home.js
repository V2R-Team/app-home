import React, { Component } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEllipsisV } from '@fortawesome/free-solid-svg-icons'
import { faComment, faHeart } from "@fortawesome/free-regular-svg-icons";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {

  }

  render() {
    return (
      <React.Fragment>
        <Container className="p-0 main_container">
          <Row className="m-0 main_row">
            <Col xs="12" className="p-0">
              <div className="trending-content">
                <span className="post_trending"><img src="/images/trending.png" className="post_trending_img" alt="Trending" /> Trending on Multiple</span>
                <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px" }} className="ellips_icon" />
                <div className="trending_post">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">Amit Sharma <span>@amitsharma</span></a>
                    <small className="site_description">4 hours ago</small>
                    <p className="site_content">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia</p>
                    <div className="post_img_box">
                      <img src="images/post_img.jpg" alt="Image" />

                    </div>
                    <ul>
                      <li><FontAwesomeIcon icon={faHeart} className="" /> 439</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> 10</li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 410</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="people_to_follow">
                <h5 className="site_heading">People to follow <a href="javascript:void(0)" className="view_all">View All</a></h5>
                <small className="site_description">Check out some people with similar interests as yours on Multipie</small>
                <div className="peoples-slide table-responsive">
                  <div className="w-max-content">
                    <div className="people_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>
                      <div className="follow_user_img"><img src="/images/profile1.png" alt="Profile" /></div>
                      <h5 className="site_heading">George Simon</h5>
                      <p className="site_description p-0">Entrepreneur + Venture Investor | Invest in te..</p>
                      <a href="javascript:void(0)" className="follow_btn">Follow</a>
                    </div>
                    <div className="people_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>
                      <div className="follow_user_img"><img src="/images/profile1.png" alt="Profile" /></div>
                      <h5 className="site_heading">George Simon</h5>
                      <p className="site_description p-0">Entrepreneur + Venture Investor | Invest in te..</p>
                      <a href="javascript:void(0)" className="follow_btn">Follow</a>
                    </div>
                    <div className="people_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>
                      <div className="follow_user_img"><img src="/images/profile1.png" alt="Profile" /></div>
                      <h5 className="site_heading">George Simon</h5>
                      <p className="site_description p-0">Entrepreneur + Venture Investor | Invest in te..</p>
                      <a href="javascript:void(0)" className="follow_btn">Follow</a>
                    </div>
                    <div className="people_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>
                      <div className="follow_user_img"><img src="/images/profile1.png" alt="Profile" /></div>
                      <h5 className="site_heading">George Simon</h5>
                      <p className="site_description p-0">Entrepreneur + Venture Investor | Invest in te..</p>
                      <a href="javascript:void(0)" className="follow_btn">Follow</a>
                    </div>
                  </div>
                </div>

              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="trending-content">
                <div className="comments-line"></div>
                <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px" }} className="ellips_icon" />
                <div className="trending_post comments_sec">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">Maria Cortez <span>@maria.cortez</span><img src="/images/verified.svg" className="ml-1" alt="verified" /></a>
                    <small className="site_description">8 hours ago</small>
                    <p className="site_content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero quam justo, adipiscing facilisi. Amet eu nisl libero eu. Lobortis lectus gravida duis</p>
                    <ul>
                      <li><FontAwesomeIcon icon={faHeart} className="" /> 41k</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> 10</li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 410</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
                <div className="trending_post ">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">Vikram Sharma <span>@vikram.sh</span></a>
                    <small className="site_description d-inline-block ml-2">4h</small>
                    <p className="site_content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero quam justo, adipiscing facilisi. Amet eu nisl libero eu. Lobortis lectus gravida duis</p>
                    <ul>
                      <li style={{ color: "#F64545" }}><img src="/images/like.svg" alt="Img" /> 5</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> </li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 1</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
                <div className='view_all_comments'>
                  <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px", color: '#c4c4c4' }} />
                  <a href="javascript:void(0)">View all 56 comments</a>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="people_to_follow">
                <h5 className="site_heading">Follow suggestions<a href="javascript:void(0)" className="view_all">View All</a></h5>
                <small className="site_description">Some companies you might be interested in </small>
                <div className="peoples-slide table-responsive">
                  <div className="w-max-content">
                    <div className="people_box sugg_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>
                      <div className="follow_sugg_img"><img src="/images/lylend.png" alt="Profile" /></div>
                      <h5 className="site_heading">Ashok Leyland</h5>
                      <a href="javascript:void(0)" className="follow_btn">Follow</a>
                    </div>
                    <div className="people_box sugg_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>
                      <div className="follow_sugg_img"><img src="/images/tata.png" alt="Profile" /></div>
                      <h5 className="site_heading">Tata Motors</h5>
                      <a href="javascript:void(0)" className="follow_btn">Follow</a>
                    </div>
                    <div className="people_box sugg_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>
                      <div className="follow_sugg_img"><img src="/images/microsoft.png" alt="Profile" /></div>
                      <h5 className="site_heading">Microsoft</h5>
                      <a href="javascript:void(0)" className="follow_btn">Follow</a>
                    </div>
                  </div>
                </div>

              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="trending-content">
                <span className="post_trending"><img src="/images/trending.png" className="post_trending_img" alt="Trending" /> Trending on Multiple</span>
                <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px" }} className="ellips_icon" />
                <div className="trending_post">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">Nina Simone <span>@nina04</span></a>
                    <small className="site_description">10 Minutes Ago</small>
                    <p className="site_content">A start to getting one of my own one day</p>
                    <div className="invest-box">
                      Invested in <span>Tesla</span>
                      <p>$TSLA <img src="/images/tesla.png" alt="img" /></p>
                    </div>
                    <ul>
                      <li><FontAwesomeIcon icon={faHeart} className="" /> 41k</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> 10</li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 410</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="trending-content">
                <span className="post_trending"><img src="/images/reacted-user.png" alt="Trending" /><img className="reacted_user" src="/images/profile1.png" alt="Trending" /> Nikhil and 1 other reacted to this</span>
                <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px" }} className="ellips_icon" />
                <div className="trending_post">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">Rahul Reddy <span>@r.reddy</span><img src="/images/verified.svg" className="ml-1" alt="verified" /></a>
                    <small className="site_description">10 hours ago</small>
                    <p className="site_content">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia</p>
                    <div className="post_img_box">
                      <img src="images/post_img1.jpg" alt="Image" />
                      <div className="img_details">
                        The neuroscience of Trust
                      <a href="javascript:void(0)">hbr.org</a>
                      </div>
                    </div>
                    <ul>
                      <li><FontAwesomeIcon icon={faHeart} className="" /> 41k</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> 10</li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 410</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="people_to_follow">
                <h5 className="site_heading">Top movers<a href="javascript:void(0)" className="view_all">View All</a></h5>
                <small className="site_description">Some companies you might be interested in </small>
                <div className="peoples-slide table-responsive">
                  <div className="w-max-content">
                    <div className="people_box sugg_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>
                      <div className="follow_sugg_img"><img src="/images/lylend.png" alt="Profile" /></div>
                      <h5 className="site_heading">Ashok Leyland</h5>
                      <a href="javascript:void(0)" className="follow_btn top_mover_btn">+25.51%</a>
                    </div>
                    <div className="people_box sugg_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>
                      <div className="follow_sugg_img"><img src="/images/tata.png" alt="Profile" /></div>
                      <h5 className="site_heading">Tata Motors</h5>
                      <a href="javascript:void(0)" className="follow_btn top_mover_btn">+32.64%</a>
                    </div>
                    <div className="people_box sugg_box">
                      <button className="close_btn"><img src="/images/cancel.svg" alt="img" /></button>

                      <div className="follow_sugg_img"><img src="/images/microsoft.png" alt="Profile" /></div>
                      <h5 className="site_heading">Microsoft</h5>
                      <a href="javascript:void(0)" className="follow_btn top_mover_btn">+45.81%</a>
                    </div>
                  </div>
                </div>

              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="trending-content">
                <span className="post_trending"><img src="/images/trending.png" className="post_trending_img" alt="Trending" /> Trending on Multiple</span>
                <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px" }} className="ellips_icon" />
                <div className="trending_post">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">Amit Sharma <span>@amitsharma</span></a>
                    <small className="site_description">4 hours ago</small>
                    <p className="site_content">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit <a href="javascript:void(0)" className="taged_user">@123name</a> Exercitation veniam consequat Duis... <a href="javascript:void(0)" className="read_more">Read more</a></p>
                    <ul>
                      <li><FontAwesomeIcon icon={faHeart} className="" /> 41k</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> 10</li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 410</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="people_to_follow">
                <h5 className="site_heading">Trending</h5>
                <small className="site_description">All what’s happening in the market</small>
                <div className="peoples-slide table-responsive">
                  <div className="w-max-content">
                    <div className="trendings-box">
                      <img src="/images/trend-img1.jpg" alt="Image" />
                      <div className="trending_content">Tata Power Company Ltd shares up 0.94% as Nift ga...</div>
                    </div>
                    <div className="trendings-box">
                      <img src="/images/trend-img2.png" alt="Image" />
                      <div className="trending_content">Adani Green Energy acquires 205 W operating solar asse...  </div>
                    </div>
                    <div className="trendings-box">
                      <img src="/images/trend-img1.jpg" alt="Image" />
                      <div className="trending_content">Share price of LIC housing Finance Ltd. increasing...</div>
                    </div>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="trending-content option-content">
                <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px" }} className="ellips_icon" />
                <div className="trending_post">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">Amit Sharma <span>@amitsharma</span></a>
                    <small className="site_description">4 hours ago</small>
                    <p className="site_content">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit <a href="javascript:void(0)" className="taged_user">@123name</a> Exercitation veniam consequat?</p>
                    <ul className="post-polls">
                      <li>Option A</li>
                      <li>Option B</li>
                      <li>Option C</li>
                    </ul>
                    <small className="site_description">Poll open for <span>01:40</span> hours</small>
                    <ul>
                      <li><FontAwesomeIcon icon={faHeart} className="" /> 41k</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> 10</li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 410</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="people_to_follow">
                <h5 className="site_heading">Topics to follow
                <a href="javascript:void(0)" className="view_all">View All</a></h5>
                <small className="site_description">Keep track of all your favourite topics</small>
                <div className="peoples-slide table-responsive">
                  <ul className="follow_topics_ul">
                    <li><img src="/images/vent-capital.png" alt="img" /> Venture Capital <a href="javascript:void(0)">+</a></li>
                    <li><img src="/images/tech.png" alt="img" /> Tech Personalities <a href="javascript:void(0)">+</a></li>
                    <li><img src="/images/food.png" alt="img" /> Food & Drinks  <a href="javascript:void(0)">+</a></li>
                    <li><img src="/images/startup.png" alt="img" /> Startups  <a href="javascript:void(0)">+</a></li>
                    <li><img src="/images/buiseness.png" alt="img" /> Business Personalities  <a href="javascript:void(0)">+</a></li>
                    <li><img src="/images/infra.png" alt="img" /> Infrastructure  <a href="javascript:void(0)">+</a></li>
                    <li><img src="/images/cred-card.png" alt="img" /> FinTech  <a href="javascript:void(0)">+</a></li>
                    <li><img src="/images/investing.png" alt="img" /> Investing  <a href="javascript:void(0)">+</a></li>
                    <li><img src="/images/green.png" alt="img" /> Green Energy  <a href="javascript:void(0)">+</a></li>
                    <li><img src="/images/travels.png" alt="img" /> Travels  <a href="javascript:void(0)">+</a></li>
                  </ul>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="trending-content option-content">
                <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px" }} className="ellips_icon" />
                <div className="trending_post">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">Amit Sharma <span>@amitsharma</span></a>
                    <small className="site_description">4 hours ago</small>
                    <p className="site_content">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit <a href="javascript:void(0)" className="taged_user">@123name</a> Exercitation veniam consequat?</p>
                    <ul className="response-polls">
                      <li><span style={{ width: '80%', background: '#4F266E' }}>Option A</span><small>80%</small></li>
                      <li><span style={{ width: '60%', background: '#B6B6B6' }}>Option B</span><small>60%</small></li>
                      <li><span style={{ width: '40%', background: '#727272' }}>Option C</span><small>40%</small></li>
                    </ul>
                    <small className="site_description">147 responses</small>
                    <ul>
                      <li><FontAwesomeIcon icon={faHeart} className="" /> 41k</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> 10</li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 410</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="trending-content option-content">
                <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px" }} className="ellips_icon" />
                <div className="trending_post">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">George Simon <span>@g.simon</span></a>
                    <small className="site_description">4 hours ago</small>
                    <p className="site_content">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia</p>
                    <div className="peoples-slide table-responsive">
                      <div className="graph_slide">
                        <div className="trend_graph"><img src="/images/graph.png" alt="Graph" /></div>
                        <div className="trend_graph"><img src="/images/graph.png" alt="Graph" /></div>
                      </div>
                    </div>
                    <ul>
                      <li><FontAwesomeIcon icon={faHeart} className="" /> 41k</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> 10</li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 410</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
            <Col xs="12" className="p-0">
              <div className="trending-content">
                <span className="post_trending"><img src="/images/retweet.svg" className="post_retweet_img" alt="Img" /> Trending on Multiple</span>
                <FontAwesomeIcon icon={faEllipsisV} style={{ width: "6px" }} className="ellips_icon" />
                <div className="trending_post">
                  <div className="post_user_img">
                    <img src="/images/profile1.png" alt="Profile" />
                  </div>
                  <div className="user_post_details">
                    <a href="javascript:void(0)" className="site_heading">Vikram Shah <span>@vikram.sh</span></a>
                    <small className="site_description">9 hours ago</small>
                    <p className="site_content">Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia</p>
                    <div className="retweet_box">
                      <div className="user_post_details">
                        <div className="retweet_tagged_img">
                          <img src="/images/profile1.png" alt="Profile" />
                        </div>
                        <a href="javascript:void(0)" className="site_heading">George Simon <span>@g.simon</span><img src="/images/verified.svg" className="ml-1" alt="verified" /></a>
                        <p className="site_content mb-0" style={{ fontSize: '12px' }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Non libero quam justo, adipiscing facilisi. Amet eu nisl libero eu. Lobortis lectus gravida duis...</p>
                        <a href="javascript:void(0)" className="read_more">View more</a>
                      </div>
                    </div>
                    <ul>
                      <li><FontAwesomeIcon icon={faHeart} className="" /> 41k</li>
                      <li><img src="/images/retweet.svg" alt="Img" /> 10</li>
                      <li><FontAwesomeIcon icon={faComment} className="" /> 410</li>
                      <li className="float-right"><img src="/images/share.svg" alt="Img" /></li>
                    </ul>
                  </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </React.Fragment>
    );
  }
}

export default Home;
