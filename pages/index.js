import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Home from '../components/Home';
import Layout from '../components/layout/Layout';

export default function DefaultPage() {
  return (
    <Layout>
      {/* Homepage */}
      <Home />
    </Layout>
  )
}
